import * as create from './smartstring.create.js';
import * as docker from './smartstring.docker.js';
import * as indent from './smartstring.indent.js';
import * as normalize from './smartstring.normalize.js';
import * as type from './smartstring.type.js';

export { create, docker, normalize, indent, type };

export { Base64, base64 } from './smartstring.base64.js';
export { Domain } from './smartstring.domain.js';
export { GitRepo } from './smartstring.git.js';
